package com.example.demo;

import com.example.demo.dbacces.services.CardService;
import com.example.demo.dbacces.services.ProductService;
import com.example.demo.dbacces.services.PurchaseService;
import com.example.demo.dto.Card;
import com.example.demo.dto.Product;
import com.example.demo.rest.jsons.JsonCard;
import com.example.demo.rest.jsons.JsonProduct;
import com.example.demo.rest.jsons.JsonPurchase;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collection;

@RunWith(SpringRunner.class)
@SpringBootTest
class CheckApplicationTests {

    @Autowired
    PurchaseService purchaseService;

    @Autowired
    ProductService productService;

    @Autowired
    CardService cardService;

    public CheckApplicationTests() {
    }

    @Test
    void getTotalCostWithDiscount() {

        Collection<JsonPurchase> purchases = new ArrayList<>();
        purchases.add(new JsonPurchase(5, JsonProduct.builder().price(10).promo(false).build()));
        purchases.add(new JsonPurchase(1, JsonProduct.builder().price(10).promo(false).build()));
        purchases.add(new JsonPurchase(2, JsonProduct.builder().price(10).promo(false).build()));
        JsonCard card = JsonCard.builder().id(1L).number(1000L).build();
        Assertions.assertEquals(purchaseService.getTotalCostWithDiscount(purchases, card), 79.2);
    }

    @Test
    void getTotalCost() {

        Collection<JsonPurchase> purchases = new ArrayList<>();
        purchases.add(new JsonPurchase(5, JsonProduct.builder().price(10).promo(false).build()));
        purchases.add(new JsonPurchase(1, JsonProduct.builder().price(10).promo(false).build()));
        purchases.add(new JsonPurchase(2, JsonProduct.builder().price(10).promo(false).build()));
        JsonCard card = JsonCard.builder().id(1L).number(1000L).build();
        Assertions.assertEquals(purchaseService.getTotalDiscount(purchases, card), 0.8);
    }

    @Test
    void findById() {
        Product product = Product.builder()
                .id(1L)
                .name("CUCUMBER")
                .price(9.5)
                .promo(false)
                .build();
        Assertions.assertEquals(productService.findById(1L), product);
    }
    @Test
    void findByNumber() {
        Card card = Card.builder()
                .id(1L)
                .number(1000L)
                .build();
        Assertions.assertEquals(cardService.findByNumber(1L), card);
    }
}
