package com.example.demo;


import come.clevertec.console.entities.Card;
import come.clevertec.console.entities.Product;
import come.clevertec.console.entities.Purchase;
import come.clevertec.console.readers.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.*;

public class ConsoleApplicationTests {

    @Test
    void checkCard() {
        Collection<Integer> cards = Arrays.asList(1234, 5678, 8907);
        Card card = new Card();
        Assertions.assertEquals(card.checkCard(cards, 1234), "1234");
    }

    @Test
    void getProduct() {
        Product product = new Product(5, "HAM", 4.9, false);
        List<Product> products = product.getPRODUCTS();
        Assertions.assertEquals(Product.getProduct(products, 5), product);
    }

    @Test
    void getTotalCostWithDiscount() {
        Collection<Purchase> purchases = new ArrayList<>();
        purchases.add(new Purchase(1, Product.builder().price(1).promo(false).build()));
        purchases.add(new Purchase(5, Product.builder().price(2).promo(true).build()));
        purchases.add(new Purchase(1, Product.builder().price(3).promo(false).build()));
        Collection<Integer> cards = Arrays.asList(1234, 5678, 8907);

        Assertions.assertEquals(Purchase.getTotalCostWithDiscount(purchases, cards, 1234), 12.87);
    }

    @Test
    void getTotalDiscount() {
        Collection<Purchase> purchases = new ArrayList<>();
        purchases.add(new Purchase(1, Product.builder().price(1).promo(false).build()));
        purchases.add(new Purchase(2, Product.builder().price(5).promo(true).build()));
        purchases.add(new Purchase(1, Product.builder().price(10).promo(false).build()));
        Collection<Integer> cards = Arrays.asList(1234, 5678, 8907);

        Assertions.assertEquals(Purchase.getTotalDiscount(purchases, cards, 1234), 0.21);
    }

    @Test
    void getData() {
        ConsoleReader reader = new ConsoleReader();
        Map<Integer, Integer> data = Map.of(5, 6, 12, 1, 1, 7);
        Scanner scanner = new Scanner("5-6 12-1 1-7");
        Assertions.assertEquals(reader.getData(scanner), data);

    }

    @Test
    void selectOption() {
        ConsoleReader reader = new ConsoleReader();
        Scanner scanner = new Scanner("1");
        Assertions.assertEquals(reader.selectOption(scanner, ""), 1);
    }

    @Test
    void getCardNumber() {
        ConsoleReader reader = new ConsoleReader();
        Scanner scanner = new Scanner("1234");
        Assertions.assertEquals(reader.getCardNumber(scanner), 1234);
    }

    @Test
    void readDataC() {
        BaseCodeReader<Collection<Integer>, Card> readCardsFromCode = new ReadCardsFromCode();
        Collection<Integer> cards = Arrays.asList(1234, 2345, 1236, 8756, 4536, 5436, 3546);
        Assertions.assertEquals(readCardsFromCode.readData(new Card()), cards);
    }

    @Test
    void readDataF() {
        BaseFileReader<Collection<Integer>, File> fileReader = new ReadCardsFromFile();
        Collection<Integer> cards = Arrays.asList(1234, 1000, 4444);
        Assertions.assertEquals(fileReader.readData(new File("./src/main/resources/test_cards.txt")), cards);
    }

    @Test
    void readDataPC() {
        BaseCodeReader<List<Product>, Product> codeReader = new ReadProductsFromCode();
        Product product = Product.builder().build();
        List<Product> products = product.getPRODUCTS();
        Assertions.assertEquals(codeReader.readData(product), products);
    }

    @Test
    void readDataPF() {
        BaseFileReader<List<Product>, File> fileReader = new ReadProductsFromFile();
        List<Product> products = Arrays.asList(
                Product.builder().id(1).name("aaaa").price(1).promo(false).build(),
                Product.builder().id(2).name("bbbb").price(2).promo(false).build(),
                Product.builder().id(3).name("cccc").price(3).promo(false).build()
        );
        Assertions.assertEquals(fileReader.readData(new File("./src/main/resources/test_product.txt")), products);
    }
}

