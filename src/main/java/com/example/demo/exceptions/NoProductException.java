package com.example.demo.exceptions;

public class NoProductException extends Exception {

    private final static String MESSAGE = "No products found with this index";

    public NoProductException() {
        super(MESSAGE);
    }

}
