package com.example.demo.rest.jsons;


import java.util.Map;


public class JsonRequest extends JsonBean<Long> {

    private final Map<Integer,Integer> idAndQuantity;
    private final JsonCard card;

    public JsonRequest(Map<Integer, Integer> idAndQuantity, JsonCard card) {
        this.idAndQuantity = idAndQuantity;
        this.card = card;
    }

    public Map<Integer, Integer> getIdAndQuantity() {
        return idAndQuantity;
    }

    public JsonCard getCard() {
        return card;
    }
}
