package com.example.demo.rest.jsons.converters;

import com.example.demo.dto.BaseBean;
import com.example.demo.rest.jsons.JsonBean;

public interface JsonBeanConverter <I extends Comparable<I>, B extends BaseBean<I>, J extends JsonBean<I>>{
    J toJsonBean(B bean);

    B toBean(J jsonBean);
}
