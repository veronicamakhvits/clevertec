package com.example.demo.rest.jsons;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonBean<I extends Comparable<I>> {

    @NotNull
    private I id;

    public JsonBean() {

    }

    public @NotNull I getId() {
        return id;
    }

    public void setId(@NotNull I id) {
        this.id = id;
    }
}
