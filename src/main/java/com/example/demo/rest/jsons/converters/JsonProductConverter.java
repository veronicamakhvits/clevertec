package com.example.demo.rest.jsons.converters;

import org.springframework.stereotype.Component;
import com.example.demo.dto.Product;
import com.example.demo.rest.jsons.JsonProduct;

@Component
public class JsonProductConverter implements JsonBeanConverter<Long, Product, JsonProduct> {
    @Override
    public JsonProduct toJsonBean(Product bean) {
        return JsonProduct.builder()
                .id(bean.getId())
                .name(bean.getName())
                .price(bean.getPrice())
                .build();
    }

    @Override
    public Product toBean(JsonProduct jsonBean) {
        return Product.builder()
                .id(jsonBean.getId())
                .name(jsonBean.getName())
                .price(jsonBean.getPrice())
                .build();
    }
}
