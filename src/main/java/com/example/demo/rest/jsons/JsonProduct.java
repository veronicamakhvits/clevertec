package com.example.demo.rest.jsons;

public class JsonProduct extends JsonBean<Long> {

    private Long id;
    private String name;
    private double price;
    private boolean promo;

    public JsonProduct(Long id, String name, double price, boolean promo) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.promo = promo;
    }

    public boolean isPromo() {
        return promo;
    }

    public void setPromo(boolean promo) {
        this.promo = promo;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public JsonProduct() {

    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {

        private Long id;
        private String name;
        private double price;
        private boolean promo;

        private Builder() {
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder price(double price) {
            this.price = price;
            return this;
        }
        public Builder promo(boolean promo) {
            this.promo = promo;
            return this;
        }

        public JsonProduct build() {
            JsonProduct jsonProduct = new JsonProduct();
            jsonProduct.setId(id);
            jsonProduct.setName(name);
            jsonProduct.setPrice(price);
            jsonProduct.setPromo(promo);
            return jsonProduct;
        }
    }
}
