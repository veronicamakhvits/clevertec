package com.example.demo.rest.jsons;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;
import java.util.Collection;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder
public class JsonCheck extends JsonBean<Long> {

    private final String shop;
    private final Integer cashier;
    private final LocalDate date;
    private final Collection<JsonPurchase> purchases;
    private final double discount;
    private final String card;
    private final double total;

    public JsonCheck(String shop, Integer cashier, LocalDate date, Collection<JsonPurchase> purchases, double discount, String card, double total) {
        this.shop = shop;
        this.cashier = cashier;
        this.date = date;
        this.purchases = purchases;
        this.discount = discount;
        this.card = card;
        this.total = total;
    }

}
