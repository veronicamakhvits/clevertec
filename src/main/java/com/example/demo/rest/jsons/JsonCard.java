package com.example.demo.rest.jsons;

public class JsonCard extends JsonBean<Long>{
    private Long id;
    private Long number;

    public JsonCard(Long id, Long number) {
        this.id = id;
        this.number = number;
    }

    public JsonCard() {

    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Long getId() {
        return id;
    }

    public Long getNumber() {
        return number;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {

        private Long id;
        private Long number;

        private Builder() {
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder number(Long number) {
            this.number = number;
            return this;
        }

        public JsonCard build() {
            JsonCard jsonCard = new JsonCard();
            jsonCard.setId(id);
            jsonCard.setNumber(number);
            return jsonCard;
        }
    }
}
