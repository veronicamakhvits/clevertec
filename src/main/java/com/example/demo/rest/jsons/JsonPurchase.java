package com.example.demo.rest.jsons;

import lombok.Data;
import lombok.EqualsAndHashCode;


@EqualsAndHashCode
@Data
public class JsonPurchase {

    private final Integer quantity;
    private final JsonProduct product;
    private final double total;

    public Integer getQuantity() {
        return quantity;
    }

    public JsonProduct getProduct() {
        return product;
    }

    public double getTotal() {
        return total;
    }

    public JsonPurchase(Integer quantity, JsonProduct product) {
        double total = quantity * product.getPrice();
        this.quantity = quantity;
        if (product.isPromo() && quantity > 4) {
            product.setName(product.getName() + "  -10%");
            this.product = product;
        } else {
            this.product = product;
        }
        this.total = total;
    }
}
