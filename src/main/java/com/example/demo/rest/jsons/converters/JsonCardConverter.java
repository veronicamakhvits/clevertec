package com.example.demo.rest.jsons.converters;

import com.example.demo.rest.jsons.JsonCard;
import org.springframework.stereotype.Component;
import com.example.demo.dto.Card;

@Component
public class JsonCardConverter implements JsonBeanConverter<Long, Card, JsonCard> {

    @Override
    public JsonCard toJsonBean(Card bean) {
        return JsonCard.builder()
                .id(bean.getId())
                .number(bean.getNumber())
                .build();
    }

    @Override
    public Card toBean(JsonCard jsonBean) {
        return Card.builder()
                .id(jsonBean.getId())
                .number(jsonBean.getNumber())
                .build();
    }
}
