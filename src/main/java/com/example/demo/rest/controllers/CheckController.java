package com.example.demo.rest.controllers;

import com.example.demo.dbacces.services.CardService;
import com.example.demo.dbacces.services.ProductService;
import com.example.demo.dbacces.services.PurchaseService;
import com.example.demo.rest.jsons.*;
import com.example.demo.rest.jsons.converters.JsonBeanConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import com.example.demo.dto.Card;
import com.example.demo.dto.Product;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

@RestController
@RequestMapping("check")
public class CheckController {

    private final CardService cardService;
    private final ProductService productService;
    private final PurchaseService purchaseService;
    private final JsonBeanConverter<Long, Product, JsonProduct> pConverter;
    private final JsonBeanConverter<Long, Card, JsonCard> cConverter;

    @Autowired
    public CheckController(CardService cardService, ProductService productService, PurchaseService purchaseService, JsonBeanConverter<Long, Product, JsonProduct> pConverter, JsonBeanConverter<Long, Card, JsonCard> cConverter) {
        this.cardService = cardService;
        this.productService = productService;
        this.purchaseService = purchaseService;
        this.pConverter = pConverter;
        this.cConverter = cConverter;
    }

    @PostMapping("/get")
    @ResponseStatus(HttpStatus.OK)
    public JsonCheck getAll(@RequestBody JsonRequest request) {

        Map<Integer, Integer> params = request.getIdAndQuantity();
        Collection<JsonPurchase> purchases = new ArrayList<>();
        JsonCard card = request.getCard();
        for (Map.Entry<Integer, Integer> entry : params.entrySet()) {
            JsonProduct jsonProduct = pConverter.toJsonBean(
                    productService.findById(Long.valueOf(entry.getKey()))
            );
            Integer id = entry.getValue();
            JsonPurchase jsonPurchase = new JsonPurchase(id, jsonProduct);
            purchases.add(jsonPurchase);
        }
        card = cConverter.toJsonBean(cardService.findByNumber(card.getNumber()));
        return JsonCheck.builder()
                .shop("SHOP")
                .cashier(12345)
                .date(LocalDate.now())
                .purchases(purchases)
                .card(card.getNumber() == 0 ? "-----" : card.getNumber().toString())
                .discount(purchaseService.getTotalDiscount(purchases, card))
                .total(purchaseService.getTotalCostWithDiscount(purchases, card))
                .build();
    }
}
