package com.example.demo.dbacces.services;

import come.clevertec.console.entities.Purchase;
import org.springframework.stereotype.Service;
import com.example.demo.rest.jsons.JsonCard;
import com.example.demo.rest.jsons.JsonPurchase;

import java.util.Collection;

@Service
public class PurchaseService implements BaseService<Purchase> {

    public PurchaseService() {
    }

    public double getTotalCostWithDiscount(Collection<JsonPurchase> purchases, JsonCard card) {
        double sum = 0;
        for (JsonPurchase p : purchases) {
            if (p.getProduct().isPromo() && p.getQuantity() > 4) {
                sum = sum + p.getTotal() * 0.9;
            } else {
                sum = sum + p.getTotal();
            }
        }
        if (card != null) {
            sum = sum * 0.99;
        }
        return Math.round(sum * 100.0) / 100.0;
    }

    public double getTotalDiscount(Collection<JsonPurchase> purchases, JsonCard card) {
        return Math.round((getTotalCost(purchases) - getTotalCostWithDiscount(purchases, card)) * 100.0) / 100.0;
    }

    private double getTotalCost(Collection<JsonPurchase> purchases) {
        double sum = 0;
        for (JsonPurchase p : purchases) {
            sum = sum + p.getTotal();
        }
        return Math.round(sum * 100.0) / 100.0;
    }

}
