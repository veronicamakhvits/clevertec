package com.example.demo.dbacces.services;

import com.example.demo.dbacces.entities.JpaProduct;
import come.clevertec.console.exceptions.NoProductException;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.dto.Product;
import com.example.demo.dbacces.converters.EntityBeanConverter;
import com.example.demo.dbacces.repository.ProductRepository;

import java.util.Optional;

@Service
public class ProductService implements BaseService<Product> {

    private final ProductRepository repository;
    private final EntityBeanConverter<Long, JpaProduct, Product> converter;

    @Autowired
    public ProductService(ProductRepository repository, EntityBeanConverter<Long, JpaProduct, Product> converter) {
        this.repository = repository;
        this.converter = converter;
    }

    @SneakyThrows
    public Product findById(Long id) {
        Optional<JpaProduct> product = repository.findById(id);
        if (product.isPresent()) {
            return converter.toBean(product.get());
        } else {
            throw new NoProductException();
        }
    }
}
