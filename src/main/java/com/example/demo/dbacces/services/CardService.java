package com.example.demo.dbacces.services;

import com.example.demo.dbacces.entities.JpaCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.dto.Card;
import com.example.demo.dbacces.converters.EntityBeanConverter;
import com.example.demo.dbacces.repository.CardRepository;

@Service
public class CardService implements BaseService<Card> {

    private final CardRepository repository;
    private final EntityBeanConverter<Long, JpaCard, Card> converter;

    @Autowired
    public CardService(CardRepository repository, EntityBeanConverter<Long, JpaCard, Card> converter) {
        this.repository = repository;
        this.converter = converter;
    }

    public Card findByNumber(Long number) {
        assert repository != null;
        JpaCard card = repository.findByNumber(number);
        if (card == null) {
            return Card.builder().id(1L).number(0L).build();
        }
        return converter.toBean(card);
    }
}
