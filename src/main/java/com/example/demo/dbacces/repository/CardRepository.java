package com.example.demo.dbacces.repository;

import com.example.demo.dbacces.entities.JpaCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardRepository extends JpaRepository<JpaCard,Long> {

    JpaCard findByNumber(Long number);
}
