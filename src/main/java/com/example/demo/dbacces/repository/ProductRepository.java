package com.example.demo.dbacces.repository;

import com.example.demo.dbacces.entities.JpaProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<JpaProduct, Long> {
}
