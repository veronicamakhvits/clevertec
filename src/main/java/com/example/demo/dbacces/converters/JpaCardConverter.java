package com.example.demo.dbacces.converters;

import com.example.demo.dbacces.entities.JpaCard;
import org.springframework.stereotype.Component;
import com.example.demo.dto.Card;

@Component
public class JpaCardConverter implements EntityBeanConverter<Long, JpaCard, Card> {

    @Override
    public Card toBean(JpaCard entity) {
        return Card.builder()
                .id(entity.getId())
                .number(entity.getNumber())
                .build();
    }

    @Override
    public JpaCard toEntity(Card dto) {
        return JpaCard.builder()
                .id(dto.getId())
                .number(dto.getNumber())
                .build();
    }
}
