package com.example.demo.dbacces.converters;

import com.example.demo.dbacces.entities.JpaProduct;
import org.springframework.stereotype.Component;
import com.example.demo.dto.Product;

@Component
public class JpaProductConverter implements EntityBeanConverter<Long, JpaProduct, Product> {
    @Override
    public Product toBean(JpaProduct entity) {
        return Product.builder()
                .id(entity.getId())
                .name(entity.getName())
                .price(entity.getPrice())
                .promo(entity.isPromo())
                .build();
    }

    @Override
    public JpaProduct toEntity(Product dto) {
        return JpaProduct.builder()
                .id(dto.getId())
                .name(dto.getName())
                .price(dto.getPrice())
                .promo(dto.isPromo())
                .build();
    }
}
