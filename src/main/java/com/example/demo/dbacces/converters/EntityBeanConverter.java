package com.example.demo.dbacces.converters;

import com.example.demo.dto.BaseBean;
import com.example.demo.dbacces.entities.BaseEntity;

public interface EntityBeanConverter<I extends Comparable<I>, E extends BaseEntity<I>, B extends BaseBean<I>> {

    B toBean(E entity);

    E toEntity(B dto);
}
