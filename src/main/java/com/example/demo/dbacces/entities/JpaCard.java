package com.example.demo.dbacces.entities;

import jakarta.persistence.*;

@Entity
@Table(name = "discount_card")
public class JpaCard extends BaseEntity<Long>{
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "number")
    private Long number;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public JpaCard() {

    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Long getNumber() {
        return number;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {

        private Long id;
        private Long number;

        private Builder() {
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder number(Long number) {
            this.number = number;
            return this;
        }

        public JpaCard build() {
            JpaCard jpaCard = new JpaCard();
            jpaCard.setId(id);
            jpaCard.setNumber(number);
            return jpaCard;
        }
    }
}
