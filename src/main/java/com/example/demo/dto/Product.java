package com.example.demo.dto;

public class Product extends BaseBean<Long> {

    private final Long id;
    private final String name;
    private final double price;
    private final boolean promo;

    public Product(Long id, String name, double price, boolean promo) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.promo = promo;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public boolean isPromo() {
        return promo;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {

        private Long id;
        private String name;
        private double price;
        private boolean promo;

        private Builder() {
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder price(double price) {
            this.price = price;
            return this;
        }
        public Builder promo(boolean promo) {
            this.promo = promo;
            return this;
        }

        public Product build() {
            return new Product(id, name, price, promo);
        }
    }
}
