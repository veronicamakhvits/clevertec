package com.example.demo.dto;

public class Card extends BaseBean<Long> {

    private final Long id;
    private final Long number;

    public Card(Long id, Long number) {
        this.id = id;
        this.number = number;
    }

    public Long getId() {
        return id;
    }

    public Long getNumber() {
        return number;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {

        private Long id;
        private Long number;

        private Builder() {
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder number(Long number) {
            this.number = number;
            return this;
        }

        public Card build() {
            return new Card(id, number);
        }
    }
}

