package come.clevertec.console;

import come.clevertec.console.entities.Card;
import come.clevertec.console.entities.Product;
import come.clevertec.console.readers.*;
import come.clevertec.console.writers.BaseWriteService;
import come.clevertec.console.writers.WriteInConsoleService;
import come.clevertec.console.writers.WriteInFileService;
import come.clevertec.console.entities.Purchase;
import lombok.SneakyThrows;

import java.io.File;
import java.util.*;

public class CheckRunner {

    private final static String PATH_FOR_CARDS = "./src/main/resources/cards.txt";
    private final static String PATH_FOR_PRODUCTS = "./src/main/resources/products.txt";
    private final static String MESSAGE_FOR_INPUT = "Enter the form of the initial data: \n 1 - from code \n 2 - from file \n";
    private final static String MESSAGE_FOR_OUTPUT = "Select the receipt output method:\n 1 - to a file, \n 2 - to the console \n";

    @SneakyThrows
    public static void main(String[] args) {
        BaseConsoleReader<Map<Integer, Integer>, Scanner> reader = new ConsoleReader();
        BaseCodeReader<List<Product>, Product> codeReaderP = new ReadProductsFromCode();
        BaseFileReader<List<Product>, File> fileReaderP = new ReadProductsFromFile();
        BaseCodeReader<Collection<Integer>, Card> codeReaderC = new ReadCardsFromCode();
        BaseFileReader<Collection<Integer>, File> fileReaderC = new ReadCardsFromFile();
        BaseWriteService writeServiceC = new WriteInConsoleService();
        BaseWriteService writeServiceF = new WriteInFileService();


        Map<Integer, Integer> data;
        int card;
        List<Product> products;
        Collection<Integer> cards;

        if (reader.selectOption(new Scanner(System.in), MESSAGE_FOR_INPUT) == 1) {
            products = codeReaderP.readData(Product.builder().build());
            cards = codeReaderC.readData(new Card());
        } else {
            products = fileReaderP.readData(new File(PATH_FOR_PRODUCTS));
            cards = fileReaderC.readData(new File(PATH_FOR_CARDS));
        }

        data = reader.getData(new Scanner(System.in));
        card = reader.getCardNumber(new Scanner(System.in));

        List<Purchase> purchases = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : data.entrySet()) {
            Purchase purchase = new Purchase(entry.getValue(), Product.getProduct(products, entry.getKey()));
            purchases.add(purchase);
        }

        if (reader.selectOption(new Scanner(System.in), MESSAGE_FOR_OUTPUT) == 1) {
            writeServiceF.write(purchases, cards, card);
        } else {
            writeServiceC.write(purchases, cards, card);
        }
    }
}
