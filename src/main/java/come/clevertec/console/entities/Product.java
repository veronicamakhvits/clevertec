package come.clevertec.console.entities;

import come.clevertec.console.exceptions.NoProductException;
import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Product extends BaseEntity<Long> {

    public final List<Product> PRODUCTS = new ArrayList<>();

    private Integer id;
    private String name;
    private double price;
    private boolean promo;

    public Product(Integer id, String name, double price, boolean promo) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.promo = promo;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setPromo(boolean promo) {
        this.promo = promo;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public boolean isPromo() {
        return promo;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private Integer id;
        private String name;
        private double price;
        private boolean promo;

        private Builder() {
        }

        public Builder id(Integer id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder price(double price) {
            this.price = price;
            return this;
        }

        public Builder promo(boolean promo) {
            this.promo = promo;
            return this;
        }

        public Product build() {
            return new Product(id, name, price, promo);
        }
    }

    private void initializationProducts() {
        PRODUCTS.add(new Product(1, "BACON", 4.5, false));
        PRODUCTS.add(new Product(2, "BEEF", 3, false));
        PRODUCTS.add(new Product(3, "CHICKEN", 8.3, false));
        PRODUCTS.add(new Product(4, "DUCK", 0.3, false));
        PRODUCTS.add(new Product(5, "HAM", 4.9, false));
        PRODUCTS.add(new Product(6, "LAMB", 12.4, false));
        PRODUCTS.add(new Product(7, "LIVER", 9.3, true));
        PRODUCTS.add(new Product(8, "MEAT", 3.9, false));
        PRODUCTS.add(new Product(9, "MUTTON", 2.3, false));
        PRODUCTS.add(new Product(10, "PATRIAE", 7.5, false));
        PRODUCTS.add(new Product(11, "PORK", 4.3, true));
        PRODUCTS.add(new Product(12, "POULTRY", 5, true));
        PRODUCTS.add(new Product(13, "SAUSAGE", 10, true));
        PRODUCTS.add(new Product(14, "TENDERLOIN", 3.4, false));
        PRODUCTS.add(new Product(15, "TURKEY", 3.9, false));
        PRODUCTS.add(new Product(16, "VEAL", 9.1, false));
        PRODUCTS.add(new Product(17, "VENISON", 12, false));
        PRODUCTS.add(new Product(18, "COD", 6.4, false));
        PRODUCTS.add(new Product(19, "MACKEREL", 13, false));
        PRODUCTS.add(new Product(20, "PIKE", 8.6, true));
    }

    public List<Product> getPRODUCTS() {
        initializationProducts();
        return PRODUCTS;
    }

    @SneakyThrows
    public static Product getProduct(List<Product> products, Integer id) {
        Product product = null;
        for (Product p : products) {
            if (p.getId().equals(id)) {
                product = p;
            }
        }
        if (product == null){
            throw new NoProductException();
        }
        return product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Double.compare(product.price, price) == 0 && promo == product.promo && Objects.equals(id, product.id) && Objects.equals(name, product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price, promo);
    }
}

