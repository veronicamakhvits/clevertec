package come.clevertec.console.entities;

import java.util.Arrays;
import java.util.Collection;

public class Card extends BaseEntity<Long> {

    private final Collection<Integer> CARDS =
            Arrays.asList(1234, 2345, 1236, 8756, 4536, 5436, 3546);

    public Collection<Integer> getCARDS() {
        return CARDS;
    }

    public String checkCard(Collection<Integer> cards, Integer card) {
        if (cards.contains(card)) {
            return card.toString();
        }
        if (card == 0) {
            return "no card";
        } else {
            return "card not found";
        }
    }
}
