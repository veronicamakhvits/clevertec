package come.clevertec.console.entities;

import java.util.Collection;

public class Purchase extends BaseEntity<Long> {

    private final int quantity;
    private final Product product;
    private final double total;

    public Purchase(int quantity, Product product) {
        double total = quantity * product.getPrice();
        this.quantity = quantity;
        if (product.isPromo() && quantity > 4) {
            product.setName(product.getName() + "  -10%");
            this.product = product;
        } else {
            this.product = product;
        }
        this.total = total;
    }

    public int getQuantity() {
        return quantity;
    }

    public Product getProduct() {
        return product;
    }

    public double getTotal() {
        return total;
    }

    public static double getTotalCostWithDiscount(Collection<Purchase> purchases, Collection<Integer> cards, Integer card) {
        double sum = 0;
        for (Purchase p : purchases) {
            if (p.getProduct().isPromo() && p.getQuantity() > 4) {
                sum = sum + p.getTotal() * 0.9;
            } else {
                sum = sum + p.getTotal();
            }
        }
        if (card != 0 && cards.contains(card)) {
            sum = sum * 0.99;
        }
        return Math.round(sum * 100.0) / 100.0;
    }

    public static double getTotalDiscount(Collection<Purchase> purchases, Collection<Integer> cards, Integer card) {
        return Math.round((getTotalCost(purchases) - getTotalCostWithDiscount(purchases, cards, card)) * 100.0) / 100.0;
    }

    private static double getTotalCost(Collection<Purchase> purchases) {
        double sum = 0;
        for (Purchase p : purchases) {
            sum = sum + p.getTotal();
        }
        return Math.round(sum * 100.0) / 100.0;
    }

    @Override
    public String toString() {
        String spaces = "                    $";
        String name = product.getName();
        if (name.contains("-10%")) {
            spaces = "                 $";
        }
        return quantity + "     " + product.getName() + spaces
                + String.format("%.2f", product.getPrice())
                + "   $" + String.format("%.2f", total);
    }
}
