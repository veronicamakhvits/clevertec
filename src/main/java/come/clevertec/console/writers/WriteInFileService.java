package come.clevertec.console.writers;

import come.clevertec.console.entities.Purchase;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Objects;

public class WriteInFileService implements BaseWriteService {
    @Override
    public void write(Collection<Purchase> purchases, Collection<Integer> cards, Integer card) throws IOException {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter("Check.txt", true));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Objects.requireNonNull(writer).append(getTopPart());
        for (Purchase p : purchases) {
            writer.append(p.toString())
                    .append("\n");
        }
        writer.append(getDownPart(purchases, cards, card));
        writer.close();
    }
}
