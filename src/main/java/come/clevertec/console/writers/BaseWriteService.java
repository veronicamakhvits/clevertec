package come.clevertec.console.writers;

import come.clevertec.console.entities.Card;
import come.clevertec.console.entities.Purchase;

import java.io.IOException;
import java.util.Collection;

public interface BaseWriteService {

    default String getTopPart() {
        return "                CASH RECEIPT            \n " +
                "               Supermarket 123 \n " +
                "CASHIER: #123                  date: 12.12.2023 \n " +
                "                               time: 12:34 \n" +
                "QTY   DESCRIPTION            PRICE     TOTAL\n";
    }

    default String getDownPart(Collection<Purchase> purchases, Collection<Integer> cards, Integer card) {
        double total = Purchase.getTotalCostWithDiscount(purchases, cards, card);
        double discount = Purchase.getTotalDiscount(purchases, cards, card);
        Card card1 = new Card();
        return "_____________________________________________________\n" +
                "discount card                               #" + card1.checkCard(cards, card) +
                "\nDISCOUNT                                    -$" + String.format("%.2f", discount) +
                "\nTOTAL                                       $" + String.format("%.2f", total);
    }

    void write(Collection<Purchase> purchases, Collection<Integer> cards, Integer card) throws IOException;
}
