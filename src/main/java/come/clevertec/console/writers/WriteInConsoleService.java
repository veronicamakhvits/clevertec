package come.clevertec.console.writers;

import come.clevertec.console.entities.Purchase;

import java.util.Collection;

public class WriteInConsoleService implements BaseWriteService {

    @Override
    public void write(Collection<Purchase> purchases, Collection<Integer> cards, Integer card) {
        System.out.println(getTopPart());
        for (Purchase p : purchases) {
            System.out.println(p);
        }
        System.out.println(getDownPart(purchases, cards, card));
    }
}
