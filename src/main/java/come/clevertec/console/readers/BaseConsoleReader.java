package come.clevertec.console.readers;

public interface BaseConsoleReader<T, S> extends Readerable<T> {

    T getData(S input);

    int selectOption(S input, String message);

    int getCardNumber(S input);

}
