package come.clevertec.console.readers;

import come.clevertec.console.entities.Card;

import java.util.Collection;

public class ReadCardsFromCode implements BaseCodeReader<Collection<Integer>, Card> {

    @Override
    public Collection<Integer> readData(Card input) {
        return input.getCARDS();
    }
}
