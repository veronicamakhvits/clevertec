package come.clevertec.console.readers;

import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

import static org.apache.commons.lang3.StringUtils.SPACE;

@NoArgsConstructor
public class ConsoleReader implements BaseConsoleReader<Map<Integer, Integer>, Scanner> {

    public static final String ONE = "1";
    public static final String TWO = "2";

    public Map<Integer, Integer> getData(Scanner scanner) {
        Map<Integer, Integer> data = new HashMap<>();
        int i = 0;
        while (i == 0) {
            System.out.print("Enter data in the format 'product code - quantity': ");
            String str = scanner.nextLine();
            String[] splitStr = str.split(SPACE);

            if (analysisEnteredData(splitStr)) {
                data = getDataInput(splitStr);
                i = 1;
            } else
                System.out.println("you entered the wrong data \nplz try again");
        }
        return data;
    }

    public int selectOption(Scanner scanner, String message) {
        boolean loop = true;
        String choice = "";
        while (loop) {
            System.out.print(message);
            choice = scanner.nextLine();
            if (!choice.equals(ONE) && !choice.equals(TWO)) {
                System.out.println("Wrong value! Enter 1 or 2");
            } else {
                loop = false;
            }
        }
        return Integer.parseInt(choice);
    }

    public int getCardNumber(Scanner scanner) {
        boolean loop = true;
        int choice = 0;
        while (loop) {
            try {
                System.out.print("Enter the discount card number. If you don't have one, enter \"0\": card-");
                choice = scanner.nextInt();
                loop = false;
            } catch (InputMismatchException e) {
                System.out.println("Invalid value! Enter a valid card number.");
            }
        }
        return choice;
    }

    private Map<Integer, Integer> getDataInput(String[] symbols) {
        Map<Integer, Integer> data = new HashMap<>();
        for (String symbol : symbols) {
            String[] s = symbol.split("-");
            data.put(Integer.parseInt(s[0]), Integer.parseInt(s[1]));
        }
        return data;
    }

    private Boolean analysisEnteredData(String[] data) {
        for (String datum : data) {
            if (!datum.matches("\\d+[-]\\d+")) {
                return false;
            }
        }
        return true;
    }
}
