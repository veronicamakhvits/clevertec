package come.clevertec.console.readers;

import come.clevertec.console.entities.BaseEntity;

public interface BaseCodeReader<T, D extends BaseEntity<Long>> extends Readerable<T> {

    T readData(D input);
}
