package come.clevertec.console.readers;

import come.clevertec.console.entities.Product;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReadProductsFromFile implements BaseFileReader<List<Product>, File> {

    private static final int ZERO = 0;

    @Override
    public List<Product> readData(File input) {
        List<Product> products = new ArrayList<>();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(input));
            String line = reader.readLine();
            while (line != null) {
                String[] product = line.split(StringUtils.SPACE);
                products.add(
                        Product.builder()
                                .id(Integer.parseInt(product[0]))
                                .name(product[1])
                                .price(Double.parseDouble(product[2]))
                                .promo(Boolean.parseBoolean(product[3]))
                                .build()
                );
                line = reader.readLine();
            }
        } catch (IOException e) {
            System.out.println("File not found, please check it.");
        } catch (NumberFormatException ex) {
            System.out.println("Incorrect data in the file.");
            System.exit(ZERO);
        }
        return products;
    }
}
