package come.clevertec.console.readers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class ReadCardsFromFile implements BaseFileReader<Collection<Integer>, File> {

    private static final int ZERO = 0;

    @Override
    public Collection<Integer> readData(File input) {
        Collection<Integer> cards = new ArrayList<>();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(input));
            String line = reader.readLine();
            while (line != null) {
                cards.add(Integer.parseInt(line));
                line = reader.readLine();
            }
        } catch (IOException e) {
            System.out.println("File not found, please check it.");
        } catch (NumberFormatException ex) {
            System.out.println("Incorrect data in the file.");
            System.exit(ZERO);
        }
        return cards;
    }
}
