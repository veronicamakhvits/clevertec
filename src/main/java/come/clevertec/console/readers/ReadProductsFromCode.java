package come.clevertec.console.readers;

import come.clevertec.console.entities.Product;

import java.util.List;

public class ReadProductsFromCode implements BaseCodeReader<List<Product>, Product> {

    @Override
    public List<Product> readData(Product products) {
        return products.getPRODUCTS();
    }
}
