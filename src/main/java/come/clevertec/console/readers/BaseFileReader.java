package come.clevertec.console.readers;

public interface BaseFileReader<T, R> extends Readerable<T> {

    T readData(R input);
}
