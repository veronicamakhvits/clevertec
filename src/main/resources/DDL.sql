CREATE TABLE IF NOT EXISTS product
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY,
    name varchar,
    price decimal,
    promo boolean,
    PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS discount_card
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY,
    number bigint,
    PRIMARY KEY(id)
);