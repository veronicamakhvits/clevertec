INSERT into product(name, price, promo)
values ('CUCUMBER', 9.5, false),
       ('NUTS', 14, false),
       ('POTATO', 3.6, false),
       ('TOMATO', 10, false),
       ('WATER', 6.8, false);

insert into discount_card (number)
values (1000),
       (6478),
       (9746);